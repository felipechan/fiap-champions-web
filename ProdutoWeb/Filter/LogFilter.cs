﻿using ProdutoWeb.DAO;
using ProdutoWeb.Models;
using System;
using System.Web.Mvc;
using System.Web.Routing;

namespace ProdutoWeb.Filter
{
    public class LogFilter: ActionFilterAttribute
    {
        public override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            Log log = new Log();
            log.DataLog = DateTime.Now;
            log.Caminho = filterContext.HttpContext.Request.Url.ToString();
            new LogDAO().Inserir(log);
        }

    }
}