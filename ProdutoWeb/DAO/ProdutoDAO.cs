﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ProdutoWeb.Models;
using System.Net.Http;
using Newtonsoft.Json;
using System.Net;

namespace ProdutoWeb.DAO
{
    public class ProdutoDAO
    {

        public IList<Evento> ListarTodos()
        {
            var _url = "https://na59.salesforce.com/services/apexrest/Evento__c";
            HttpClient client = new HttpClient();
            client.DefaultRequestHeaders.Authorization =
                new System.Net.Http.Headers.AuthenticationHeaderValue("Bearer", GetToken());
            HttpResponseMessage response = client.GetAsync(_url).Result;
            //if (!response.IsSuccessStatusCode) new ServiceExceptions().MappingException(response);
            string conteudoResposta = response.Content.ReadAsStringAsync().Result;
            var retorno = JsonConvert.DeserializeObject<IList<Evento>>(conteudoResposta);

            return retorno;
        }


        public String GetToken()
        {

            var _securityKey = "tiZSEfHaOfTl9bui9s5HpevN"; // Recebido por email
            var _clientSecret = "3153064188916911320";
            var _clientId = "3MVG9zlTNB8o8BA2wskfNCZLwK3IH8f1YAKKhpLtWDRFxJwaIlbotHvmVkVBNQmCRNin29zeL36OUqJ7kD8oq";
            var _redirectUri = "http://www.fiap.com.br";
            var _grantAcess = "password";
            var _userName = "felipe_s.c1@hotmail.com";
            var _password = "fiap1234" + _securityKey;
            var _urlSalesForceAuth = "https://login.salesforce.com/services/oauth2/token";

            var parameters = new Dictionary<string, string> {
                { "client_id", _clientId },
                { "client_secret", _clientSecret },
                { "redirect_uri" , _redirectUri },
                { "grant_type" , _grantAcess },
                { "username" , _userName },
                { "password" , _password },
            };

            var encodedContent = new FormUrlEncodedContent(parameters);

            ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;

            HttpClient client = new HttpClient();
            var rp = client.PostAsync(_urlSalesForceAuth, encodedContent);

            var response = rp.Result;

            if (response.IsSuccessStatusCode)
            {
                var conteudoResposta = response.Content.ReadAsStringAsync().Result;
                dynamic json = Newtonsoft.Json.JsonConvert.DeserializeObject(conteudoResposta);

                return json.access_token;
            }
            else
            {
                throw new Exception(response.ReasonPhrase);
            }

        }

        //public IList<Produto> ListarTodos()
        //{
        //    return new DataBaseContext().Produto.ToList<Produto>();
        //}


        public Produto ConsultarPorId(int Id)
        {
            return new DataBaseContext().Produto.Find(Id);
        }


        public Produto ConsultarPorNome(String Nome)
        {

            Produto Produto = new Produto();

            using (DataBaseContext DBContext = new DataBaseContext() )
            {
                Produto = DBContext.Produto.Where(p => p.NomeProduto == Nome).SingleOrDefault<Produto>();
            }

            return Produto;

        }


        public void Inserir(Produto Prod)
        {

            Prod.Ativo = true;
            using (DataBaseContext DBContext = new DataBaseContext())
            {
                DBContext.Produto.Add(Prod);
                DBContext.SaveChanges();
            }

        }

        public void Editar(Produto Prod)
        {

            using (DataBaseContext DBContext = new DataBaseContext())
            {
                DBContext.Entry(Prod).State = System.Data.Entity.EntityState.Modified;
                DBContext.SaveChanges();
            }

        }


        public void Excluir(int Id)
        {
            using (DataBaseContext DBContext = new DataBaseContext())
            {
                Produto Produto = ConsultarPorId(Id);
                DBContext.Entry(Produto).State = System.Data.Entity.EntityState.Deleted;
                DBContext.SaveChanges();
            }
            
        }



    }
}