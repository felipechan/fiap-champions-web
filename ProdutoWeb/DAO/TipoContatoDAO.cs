﻿using Oracle.ManagedDataAccess.Client;
using ProdutoWeb.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ProdutoWeb.DAO
{
    public class TipoContatoDAO
    {

        public IList<TipoContato> ListarTodos()
        {

            /*
            // Criando Objetos
            IList<TipoContato> ListaTipoContato = new List<TipoContato>();
            String QUERY = "SELECT IDTIPOCONTATO, NOMETIPOCONTATO  FROM TIPOCONTATO";

            // CONEXAO COM O BANCO DE DADOS
            OracleConnection Connection = new OracleConnection();
            Connection.ConnectionString = 
                System.Configuration.ConfigurationManager.ConnectionStrings["OracleDB"].ConnectionString;
            Connection.Open();

            // EXECUTANDO A QUERY
            OracleCommand Command = new OracleCommand(QUERY, Connection);
            OracleDataReader Reader = Command.ExecuteReader();
            while(Reader.Read())
            {
                TipoContato TipoContato = new TipoContato();
                TipoContato.IdTipoContato = int.Parse(Reader["IDTIPOCONTATO"].ToString());
                TipoContato.NomeTipoContato = Reader["NOMETIPOCONTATO"].ToString();
                ListaTipoContato.Add(TipoContato);
            }

            // Fechando as Conexões
            Reader.Close();
            Connection.Close();

            return ListaTipoContato;
            */


            DAO.DataBaseContext ctx = new DAO.DataBaseContext();
            IList<TipoContato> ListaTipoContato = ctx.TipoContato.ToList<TipoContato>();
            return ListaTipoContato;

        }



        public TipoContato BuscaPorId(int Id)
        {

            // Criando Objetos
            TipoContato Tipo = new TipoContato();
            String QUERY = "SELECT IDTIPOCONTATO, NOMETIPOCONTATO  FROM TIPOCONTATO WHERE IDTIPOCONTATO = :id";

            // CONEXAO COM O BANCO DE DADOS
            OracleConnection Connection = new OracleConnection();
            Connection.ConnectionString = System.Configuration.ConfigurationManager.ConnectionStrings["OracleDB"].ConnectionString;
            Connection.Open();

            // EXECUTANDO A QUERY
            OracleCommand Command = new OracleCommand(QUERY, Connection);
            Command.Parameters.Add("id", Id);

           
            OracleDataReader Reader = Command.ExecuteReader();
            while (Reader.Read())
            {
                Tipo.IdTipoContato = int.Parse(Reader["IDTIPOCONTATO"].ToString());
                Tipo.NomeTipoContato = Reader["NOMETIPOCONTATO"].ToString();
            }

            // Fechando as Conexões
            Reader.Close();
            Connection.Close();

            return Tipo;


        }


        public void Inserir(TipoContato Tipo)
        {

            String QUERY = "INSERT INTO TIPOCONTATO (NOMETIPOCONTATO) VALUES (:nome)";

            // CONEXAO COM O BANCO DE DADOS
            OracleConnection Connection = new OracleConnection();
            Connection.ConnectionString = System.Configuration.ConfigurationManager.ConnectionStrings["OracleDB"].ConnectionString;
            Connection.Open();

            // EXECUTANDO A QUERY
            OracleCommand Command = new OracleCommand(QUERY, Connection);
            Command.Parameters.Add("nome", Tipo.NomeTipoContato);
            Command.ExecuteNonQuery();
            
            Connection.Close();

        }


        public void Excluir(int Id)
        {

            String QUERY = "DELETE FROM TIPOCONTATO WHERE  IDTIPOCONTATO = :id ";

            // CONEXAO COM O BANCO DE DADOS
            OracleConnection Connection = new OracleConnection();
            Connection.ConnectionString = System.Configuration.ConfigurationManager.ConnectionStrings["OracleDB"].ConnectionString;
            Connection.Open();

            // EXECUTANDO A QUERY
            OracleCommand Command = new OracleCommand(QUERY, Connection);
            Command.Parameters.Add("id", Id);
            Command.ExecuteNonQuery();

            Connection.Close();

        }


        public void Alterar(TipoContato Tipo)
        {

            String QUERY = "UPDATE TIPOCONTATO SET NOMETIPOCONTATO = :nome WHERE  IDTIPOCONTATO = :id ";

            // CONEXAO COM O BANCO DE DADOS
            OracleConnection Connection = new OracleConnection();
            Connection.ConnectionString = System.Configuration.ConfigurationManager.ConnectionStrings["OracleDB"].ConnectionString;
            Connection.Open();

            // EXECUTANDO A QUERY
            OracleCommand Command = new OracleCommand(QUERY, Connection);
            Command.Parameters.Add("nome", Tipo.NomeTipoContato);
            Command.Parameters.Add("id", Tipo.IdTipoContato);
            Command.ExecuteNonQuery();

            Connection.Close();

        }

    }
}
 
 
 