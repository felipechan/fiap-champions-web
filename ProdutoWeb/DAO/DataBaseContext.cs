﻿using ProdutoWeb.Models;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration.Conventions;

namespace ProdutoWeb.DAO
{
    public class DataBaseContext : DbContext
    {

        public DataBaseContext() : base("name=OracleDbContext")
        {
            Database.SetInitializer<DataBaseContext>(null);
        }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.HasDefaultSchema("PF0954");
            modelBuilder.Conventions.Remove<PluralizingTableNameConvention>();
        }

        public DbSet<TipoContato> TipoContato { get; set; }
        public DbSet<Produto> Produto { get; set; }
        public DbSet<Log> Log { get; set; }

    }
}

