﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;

namespace ProdutoWeb.DAO
{
    public class Auth
    {

        public Auth()
        {

        }

        public String GetToken()
        {

            var _securityKey = "tiZSEfHaOfTl9bui9s5HpevN"; // Recebido por email
            var _clientSecret = "3153064188916911320";
            var _clientId = "3MVG9zlTNB8o8BA2wskfNCZLwK3IH8f1YAKKhpLtWDRFxJwaIlbotHvmVkVBNQmCRNin29zeL36OUqJ7kD8oq";
            var _redirectUri = "http://www.fiap.com.br";
            var _grantAcess = "password";
            var _userName = "felipe_s.c1@hotmail.com";
            var _password = "fiap1234" + _securityKey;
            var _urlSalesForceAuth = "https://login.salesforce.com/services/oauth2/token";

            var parameters = new Dictionary<string, string> {
                { "client_id", _clientId },
                { "client_secret", _clientSecret },
                { "redirect_uri" , _redirectUri },
                { "grant_type" , _grantAcess },
                { "username" , _userName },
                { "password" , _password },
            };

            var encodedContent = new FormUrlEncodedContent(parameters);

            ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12; 

            HttpClient client = new HttpClient();
            var rp = client.PostAsync(_urlSalesForceAuth, encodedContent);

            var response = rp.Result;

            if (response.IsSuccessStatusCode)
            {
                var conteudoResposta = response.Content.ReadAsStringAsync().Result;
                dynamic json = Newtonsoft.Json.JsonConvert.DeserializeObject(conteudoResposta);

                return json.access_token;
            }
            else
            {
                throw new Exception(response.ReasonPhrase);
            }

        }


    }
}