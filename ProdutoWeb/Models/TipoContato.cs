﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace ProdutoWeb.Models
{
    [Table("TIPOCONTATO")]
    public class TipoContato
    {
        [Key]
        [Column("IDTIPOCONTATO")]
        public int IdTipoContato { get; set; }

        [Column("NOMETIPOCONTATO")]
        public string NomeTipoContato { get; set; }

    }

}