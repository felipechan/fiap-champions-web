﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace ProdutoWeb.Models
{
    public class Evento
    {
        [Key]
        [JsonProperty("Id")]
        public String Id { get; set; }

        [JsonProperty("NomeEvento__c")]
        public String Nome { get; set; }

        [JsonProperty("Descricao__c")]
        public String Descricao { get; set; }

        [JsonProperty("Local__c")]
        public String Local { get; set; }

        [JsonProperty("Preco__c")]
        public String Preco { get; set; }

    }
}