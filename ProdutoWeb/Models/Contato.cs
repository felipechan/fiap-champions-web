﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;

namespace ProdutoWeb.Models
{
    public class Contato
    {
        
        [Key]
        public int IdContato { get; set; }

        [Display(Name = "Nome", Description = "Digite o nome")]
        [Required(ErrorMessage = "Nome obrigatório")]
        [StringLength(50, MinimumLength = 3, ErrorMessage = "O campo Nome deve possuir no mínimo 3 e no máximo 50 caracteres")]
        [RegularExpression(@"^[a-zA-Z''-'\s]{1,40}$", ErrorMessage = "Digite o nome sem números e caracteres especiais.")]
        public string Nome { get; set; }

        [Required(ErrorMessage = "e-Mail obrigatório")]
        [EmailAddress]
        public string Email { get; set; }

        [Display(Name = "Mensagem", Description = "Digite o texto da mensagem")]
        [Required(ErrorMessage = "Mensagem Obrigatória")]
        [StringLength(2048, MinimumLength = 3, ErrorMessage = "O campo Nome deve possuir no mínimo 3 e no máximo 2048 caracteres")]
        public string Mensagem { get; set; }
         

        public TipoContato TipoContato { get; set; }


    }
}