﻿using ProdutoWeb.DAO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ProdutoWeb.Controllers
{
    public class HomeController : Controller
    {
        // GET: Home
        public ActionResult Index()
        {
            EventoDAO ev = new EventoDAO();
            ev.ListarEventos();
            return View();
        }
    }
}