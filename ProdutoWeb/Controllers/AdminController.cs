﻿using ProdutoWeb.Filter;
using ProdutoWeb.Models;
using System.Web.Mvc;

namespace ProdutoWeb.Controllers
{

    [LogFilter]
    public class AdminController : Controller
    {
        // GET: Admin
        public ActionResult Index()
        {
            return View();
        }


        [HttpPost]
        public ActionResult Login(Usuario UsuarioLogin)
        {

            if (UsuarioLogin != null)
            {                                               
                if ( UsuarioLogin.EmailUsuario.Equals("admin") ) {
                    // Adicionando o usuário na Sessão
                    Session["UsuarioLogado"] = UsuarioLogin;
                    return View();
                }  else
                {
                    TempData["Mensagem"] = "Usuário ou Senha inválida.";
                    return View("Index", UsuarioLogin);
                }
            }

            // Falha no Login
            else
            {
                TempData["Mensagem"] = "Usuário ou Senha inválida.";
                return View("Index", UsuarioLogin);
            }

        }

    }
}