﻿using ProdutoWeb.Models;
using System.Collections.Generic;
using System.Web.Mvc;

namespace ProdutoWeb.Controllers
{
    public class TipoContatoController : Controller
    {
        // GET: TipoContato
        public ActionResult Index()
        {

            // TRECHO DE SIMULAÇÃO DO BANCO DE DADOS
            // MOCK
            IList<TipoContato> ListaTipoContato = new List<TipoContato>();

            TipoContato TipoContato1 = new TipoContato();
            TipoContato1.IdTipoContato = 1;
            TipoContato1.NomeTipoContato = "Cliente";
            ListaTipoContato.Add(TipoContato1);

            TipoContato TipoContato2 = new TipoContato();
            TipoContato2.IdTipoContato = 2;
            TipoContato2.NomeTipoContato = "Investidor";
            ListaTipoContato.Add(TipoContato2);
            // FIM MOCK


            // ENVIANDO A LISTA PARA O CONTROLLER
            ViewBag.ListaTipoContato = ListaTipoContato;


            return View();
        }
    }
}