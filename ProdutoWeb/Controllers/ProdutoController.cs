﻿using ProdutoWeb.DAO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ProdutoWeb.Models;
using ProdutoWeb.Filter;

namespace ProdutoWeb.Controllers
{

    //[AcessoFilter]
    public class ProdutoController : Controller
    {

        
        [HttpGet]
        public ActionResult Index()
        {
            ProdutoDAO ProdDAO = new ProdutoDAO();
            IList<Models.Evento> lista = ProdDAO.ListarTodos();
            return View(lista);

            /*
            if (Session["UsuarioLogado"] != null) {
                ProdutoDAO ProdDAO = new ProdutoDAO();
                IList<Models.Produto> lista = ProdDAO.ListarTodos();
                return View(lista);
            }
            else
            {
                TempData["Mensagem"] = "Sessão Inválida";
                return RedirectToAction("Index", "Admin");
            }
            */


        }

       

        [HttpGet]
        public ActionResult Inserir()
        {
            ModelState.Clear();
            return View(new Produto());
        }



        [HttpPost]
        public ActionResult Inserir(Produto Prod)
        {
            bool sucesso = true;

            if ( ModelState.IsValid ) { 

                ProdutoDAO ProdDAO = new ProdutoDAO();

                // Consultando produto já Existente
                Produto ProdExistente = ProdDAO.ConsultarPorNome(Prod.NomeProduto);
                if ( (ProdExistente == null) || (!String.Equals(Prod.NomeProduto, ProdExistente.NomeProduto)) )
                {
                    // Produtos são diferentes
                    ProdDAO.Inserir(Prod);

                    // Colocando na Temp Data
                    TempData["mensagem"] = "Produto inserido com sucesso";
                    
                } else
                {
                    ModelState.AddModelError("", "Produto duplicado");
                    sucesso = false;
                }

            } else
            {
                sucesso = false;
            }


            // Tratando o Redirecionamento
            if (sucesso)
            {
                return RedirectToAction("Index");
            } else
            {
                return View();
            }


        } // Fim Inserir



        [HttpGet]
        public ActionResult Editar(int Id)
        {
            return View(new ProdutoDAO().ConsultarPorId(Id));
        }



        [HttpPost]
        public ActionResult Editar(Produto Prod)
        {
            bool sucesso = true;

            if (ModelState.IsValid)
            {

                ProdutoDAO ProdDAO = new ProdutoDAO();

                // Consultando produto já Existente
                Produto ProdExistente = ProdDAO.ConsultarPorNome(Prod.NomeProduto);
                if ((ProdExistente == null) || (!String.Equals(Prod.NomeProduto, ProdExistente.NomeProduto)))
                {
                    // Produtos são diferentes
                    ProdDAO.Editar(Prod);

                    // Colocando na Temp Data
                    TempData["mensagem"] = "Produto editado com sucesso";

                }
                else
                {
                    ModelState.AddModelError("", "Produto duplicado");
                    sucesso = false;
                }

            }
            else
            {
                sucesso = false;
            }


            // Tratando o Redirecionamento
            if (sucesso)
            {
                return RedirectToAction("Index");
            }
            else
            {
                return View();
            }


        } // Fim Inserir



        [HttpGet]
        public ActionResult Consultar(int Id)
        {
            return View(new ProdutoDAO().ConsultarPorId(Id));
        }


        [HttpGet]
        public ActionResult Excluir(int Id)
        {
            new ProdutoDAO().Excluir(Id);
            TempData["mensagem"] = "Produto excluido com sucesso";
            return RedirectToAction("Index");
        }



    }
}